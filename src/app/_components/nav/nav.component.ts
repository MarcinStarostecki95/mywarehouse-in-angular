import { Component, AfterViewInit } from '@angular/core';
import { startWith, delay } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements AfterViewInit {

  subscription: Subscription;
  authentication: boolean;
  isAdminSubscription: Subscription;
  isAdmin: boolean;
  title = 'Angular 5 Seed';

  constructor(private helpers: MyHelpers) {

  }

  ngAfterViewInit() {

    this.subscription = this.helpers.isAuthenticationChanged().pipe(
      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );

    this.isAdminSubscription = this.helpers.isAdminChanged().pipe(
        startWith(this.helpers.isAdmin()),
        delay(0)).subscribe((value) =>
        this.isAdmin = value
      );

    console.log('NavComponent => ngAfterViewInit() => isAdmin: ' + this.isAdmin);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.isAdminSubscription.unsubscribe();
  }
}

