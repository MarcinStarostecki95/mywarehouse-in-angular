import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { $ } from 'protractor';

@Injectable()
export class MyHelpers  {
    private authenticationChanged = new Subject<boolean>();
    private adminChanged = new Subject<boolean>();
    public jwtHelper: JwtHelperService = new JwtHelperService(); // musi być jako nowy object, ineczej jest błąd
    constructor() {}

    public isAuthenticated(): boolean {
      // console.log('MyHelpers/isAuthenticated() = ' + !window.localStorage.token);
      return (!(window.localStorage.token === undefined ||
            window.localStorage.token === null ||
            window.localStorage.token === 'null' ||
            window.localStorage.token === 'undefined' ||
            window.localStorage.token === ''));
    }

    public isAdmin(): boolean {
      if (!this.isAuthenticated()) {
        return false;
      } else {
        const obj = JSON.parse(window.localStorage.token);
        console.log('MyHelpers/isAdmin() window.localStorage.token.isAdmin = ' + obj.email + ' ' + !!obj.isAdmin);
        return !!obj.isAdmin;
      }
  }

    public isAuthenticationChanged(): any {
        return this.authenticationChanged.asObservable();
    }

    public isAdminChanged(): any {
      return this.adminChanged.asObservable();
    }

    public getToken(): any {
        if ( window.localStorage.token === undefined ||
            window.localStorage.token === null ||
            window.localStorage.token === 'null' ||
            window.localStorage.token === 'undefined' ||
            window.localStorage.token === '') {
            return '';
        }
        const obj = JSON.parse(window.localStorage.token);
        // console.log('myHelper => getToken() json.parse() ' + obj.token);
        return obj.token;
    }

    public getUserId(): any {
      if ( window.localStorage.token === undefined ||
          window.localStorage.token === null ||
          window.localStorage.token === 'null' ||
          window.localStorage.token === 'undefined' ||
          window.localStorage.token === '') {
          return '';
      }
      const obj = JSON.parse(window.localStorage.token);
      console.log('userId = ' + obj.userId);
      return obj.userId;
  }

    // public getRolesFromToken() {
    //     if (this.isAuthenticated()) {
    //       const token = this.getToken();
    //       const decodedToken = this.jwtHelper.decodeToken(token);
    //       console.log(decodedToken);
    //       const userRoles = decodedToken.roles;
    //       console.log('userRoles: ' + userRoles);
    //       if (userRoles instanceof Array) {
    //         console.log('array');
    //         console.log('roles: ' + userRoles);
    //         console.log('length: ' + userRoles.length);
    //         return userRoles;
    //       } else {
    //         const roles: string[] = [userRoles];
    //         console.log('not array');
    //         console.log('roles: ' + roles);
    //         console.log('length: ' + roles.length);
    //         return roles;
    //       }
    //     }
    //     console.log('getRolesFromToken() token is null');
    // }

    public setToken( data: any ): void {
        this.setStorageToken(JSON.stringify(data));
    }

    public failToken(): void {
        this.setStorageToken(undefined);
    }

    public logout(): void {
        this.setStorageToken(undefined);
    }

    private setStorageToken(value: any): void {
        window.localStorage.token = value;
        this.authenticationChanged.next(this.isAuthenticated());
        this.adminChanged.next(this.isAdmin());
    }
}
