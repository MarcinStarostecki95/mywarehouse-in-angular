import { AuthenticationService } from './../../_services/authentication.service';
import { AuthenticationRouting } from './authentication.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterResultComponent } from './components/register-result/register-result.component';



@NgModule({
  declarations: [LoginComponent, RegisterComponent, LogoutComponent, RegisterResultComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AuthenticationRouting
  ],
  providers:    [ AuthenticationService ]
})
export class AuthenticationModule { }
