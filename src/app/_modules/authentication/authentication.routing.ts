import { RegisterResultComponent } from './components/register-result/register-result.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from './components/logout/logout.component';


const authenticationRoutes: Routes = [
  {
    path: 'login',
    children: [
      { path: '', component: LoginComponent }
    ]
  },
  {
    path: 'logout',
    children: [
      { path: '', component: LogoutComponent }
    ]
  },
  {
    path: 'register',
    children: [
      { path: '', component: RegisterComponent }
    ]
  },
  {
    path: 'registerResult',
    children: [
      { path: '', component: RegisterResultComponent }
    ]
  }
];

export const AuthenticationRouting = RouterModule.forChild(authenticationRoutes);
