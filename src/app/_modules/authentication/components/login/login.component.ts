import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/_services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm;

  // tslint:disable-next-line:max-line-length
  constructor(
    private helpers: MyHelpers,
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
   }

  ngOnInit(): void {
  }

  login(formData: any): void {
    const user = formData.value;
    console.log('email: ' + user.email + ';  password: ' + user.password);
    const authValues = {email: user.email, password: user.password};
    this.authenticationService.login(authValues).subscribe(response => {
      console.log('login => response.token: ' + response.token);
      console.log('login => response.userId: ' + response.userId);
      this.helpers.setToken(response);
      this.router.navigate(['/rental']);
    });
  }

}
