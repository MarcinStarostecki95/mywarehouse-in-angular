import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';

@Component({
  selector: 'app-logout',
  template: '<ng-content></ng-content>'
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private helpers: MyHelpers) { }

  ngOnInit() {
    this.helpers.logout();
    this.router.navigate(['/login']);
  }

}
