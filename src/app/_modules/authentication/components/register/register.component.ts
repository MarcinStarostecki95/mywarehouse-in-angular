import { Component, OnInit } from '@angular/core';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm;

  // tslint:disable-next-line:max-line-length
  constructor(
    private helpers: MyHelpers,
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService) {
    this.registerForm = this.formBuilder.group({
      email: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: ''
    });
   }

  ngOnInit(): void {
  }

  register(formData: any): void {
    const user = formData.value;
    console.log('email: ' + user.email + ';  password: ' + user.password);
    const formValues = {email: user.email, password: user.password, firstName: user.firstName, lastName: user.lastName};
    this.authenticationService.register(formValues).subscribe((response: HttpResponse<any>) => {
      // console.log('register: ' + response.code + '  ' + response.description);
      console.log('register: ' + response);
      this.router.navigate(['/registerResult']);
    });
  }

}
