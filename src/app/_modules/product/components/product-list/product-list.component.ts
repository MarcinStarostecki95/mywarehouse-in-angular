import { HttpResponse } from '@angular/common/http';
import { Product } from './../../models/product';
import { ProductService } from './../../../../_services/product.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  headers = ['#', 'Photo', 'Name', 'Description', 'Actions'];
  products: any;
  // localProducts = [
  //   {"id": "5eac09fb98aada40e441170c",
  //   "name": "Okulary ochronnee",
  //  "category": "Test",
  //  "price": 11111,
  //  "ownerId": "000000000000000000000000",
  //  "photoUrl": "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/O/k/Okulary_ochronne_3M_Virtua_7100140622-216647-527400.webp",
  //  "description":"Okulary ochronne firmy 3M Virtua",
  //  "categoryId": "category 1",
  //  "keywords": "any",
  //  "resources": "any"
  //  },
  //  {"id": "5eac104498aada40e4411781",
  //      "name": "Wkrętaki",
  //     "category": "Test",
  //     "price": 11111,
  //     "ownerId": "000000000000000000000000",
  //     "photoUrl": "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/Z/e/Zestaw_wkretakow_VDE_7_szt.-213791-397195_1.webp",
  //     "description":"Okulary ochronne firmy 3M Virtua",
  //     "categoryId": "category 1",
  //     "keywords": "any",
  //     "resources": "any"
  //     }];
  selectedProduct: Product;
  productToEdit: Product;
  editForm;
  deleteForm;
  closeResult: string;
  mySubscription: any;

  constructor(
    private productService: ProductService,
    private router: Router,
    private formBuilder: FormBuilder,
    @Inject(DOCUMENT) private document: Document) {
      this.editForm = this.formBuilder.group({
        id: '',
        name: '',
        photoUrl: '',
        description: ''
      });
      this.deleteForm = this.formBuilder.group({
        id: '',
        name: '',
        photoUrl: '',
        description: ''
      });
     }

  ngOnInit(): void {
    this.getAll();
    // this.getLocal();
  }

  getAll(): void {
    this.productService.getAll()
      .subscribe(response => {
        this.products = response.products;
      });
  }
  getLocal(): void {
    // this.products = this.localProducts;
  }

  public onSelect(item: any): void {
    this.selectedProduct = item;
    console.log('onSelect()' + this.selectedProduct.name);
  }

  update(formData: any): void {
    const product = formData.value;
    console.log(product.id + '   ' + product.name + '  ' + product.photoUrl + '  ' + product.description);
    const productValues = {productId: product.id, name: product.name, url: product.photoUrl, description: product.description};
    this.editForm.reset();
    this.productService.update(productValues).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('update => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      this.getAll();
    }, 200);
  }

  delete(formData: any): void {
    const product = formData.value;
    console.log(product.id + '   ' + product.name + '  ' + product.photoUrl + '  ' + product.description);
    this.editForm.reset();
    this.productService.delete(product.id).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('delete => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      this.getAll();
    }, 200);
    this.products = this.products.filter(p => p.id !== product.id);
  }
}
