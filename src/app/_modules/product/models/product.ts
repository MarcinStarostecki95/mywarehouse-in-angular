export class Product {
  id: string;
  name: string;
  category: string;
  price: number;
  ownerId: string;
  photoUrl: string;
  description: string;
  categoryId: string;
  keywords: any;
  resources: any;
}
