import { ProductListComponent } from './components/product-list/product-list.component';
import { Routes, RouterModule } from '@angular/router';


const productRoutes: Routes = [
  {
    path: 'productlist',
    children: [
      { path: '', component: ProductListComponent }
    ]
  }
];

export const ProductRouting = RouterModule.forChild(productRoutes);
