import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductstoreservationlistComponent } from './productstoreservationlist.component';

describe('ProductstoreservationlistComponent', () => {
  let component: ProductstoreservationlistComponent;
  let fixture: ComponentFixture<ProductstoreservationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductstoreservationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductstoreservationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
