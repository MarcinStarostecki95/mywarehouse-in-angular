import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProductService } from 'src/app/_services/product.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { startWith, delay, first } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-productstoreservationlist',
  templateUrl: './productstoreservationlist.component.html',
  styleUrls: ['./productstoreservationlist.component.scss']
})
export class ProductstoreservationlistComponent implements AfterViewInit {
  searchForm;
  subscription: Subscription;
  authentication: boolean;
  products: any;
  selectedProduct: any;
  headers = ['ID', 'Name', 'Age', 'Gender', 'Country'];
  localProducts = [
    {"id": "5eac09fb98aada40e441170c",
    "name": "Okulary ochronnee",
   "category": "Test",
   "price": 11111,
   "ownerId": "000000000000000000000000",
   "photoUrl": "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/O/k/Okulary_ochronne_3M_Virtua_7100140622-216647-527400.webp",
   "description":"Okulary ochronne firmy 3M Virtua",
   "categoryId": "category 1",
   "keywords": "any",
   "resources": "any"
   },
   {"id": "5eac104498aada40e4411781",
       "name": "Wkrętaki",
      "category": "Test",
      "price": 11111,
      "ownerId": "000000000000000000000000",
      "photoUrl": "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/Z/e/Zestaw_wkretakow_VDE_7_szt.-213791-397195_1.webp",
      "description":"Okulary ochronne firmy 3M Virtua",
      "categoryId": "category 1",
      "keywords": "any",
      "resources": "any"
      }];

  // tslint:disable-next-line:max-line-length
  constructor(private formBuilder: FormBuilder, private productService: ProductService, private router: Router, private helpers: MyHelpers) {
    this.searchForm = this.formBuilder.group({
      query: '',
      categoryId: ''
    });
   }
  ngAfterViewInit(): void {
    this.subscription = this.helpers.isAuthenticationChanged().pipe(
      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );
    // this.getAll();
    this.getRandomProduct(10);
    // this.getLocal();
  }

  // ngOnInit(): void {
  //   this.getAll();
  //   // this.getLocal();
  // }

  getAll(): void {
    this.productService.getAll()
      .subscribe(response => {
        this.products = response.products;
      });
  }

  getRandomProduct(count: number): void {
    this.productService.GetRandomProduct(count)
      .subscribe(response => {
        this.products = response.products;
      });
  }

  getLocal(): void {
    this.products = this.localProducts;
  }

  public onSelect(item: any): void {
    this.selectedProduct = item;
    console.log('onSelect()' + this.selectedProduct.name);
  }


  public reserve(): void {
    console.log('reserve()' + this.selectedProduct.id);
    // this.router.navigate(['/reservation']);
    this.router.navigate(['reservation', this.selectedProduct.id]).then( (e) => {
      if (e) {
        console.log('Navigation is successful!');
      } else {
        console.log('Navigation has failed!');
      }
    });
  }

  searchProduct(formData: any): void {
    const data = formData.value;
    console.log(data);
    // this.searchForm.reset();
    this.productService.searchProduct(data.query, data.categoryId).subscribe(response  => {
      console.log('searchProduct() => response: ' + JSON.stringify(response.products));
      setTimeout(() => {
        this.products = response.products;
      }, 200);
    });
    // this.products = this.products.filter(p => p.id !== product.id);

  }

    // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
      this.subscription.unsubscribe();
    }

}
