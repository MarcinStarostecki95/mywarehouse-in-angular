import { RentalInputModel } from './../../models/rentalInputModel';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/_services/product.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/_modules/product/models/product';
import { RentalService } from 'src/app/_services/rental.service';
import * as moment from 'moment'; // add this 1 of 4
import { HttpResponse } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  reservationForm;
  product;
  rentalInputModel: RentalInputModel;
//   tempProduct =    {"id": "5eac09fb98aada40e441170c",
//   "name": "Okulary ochronnee",
//  "category": "Test",
//  "price": 11111,
//  "ownerId": "000000000000000000000000",
//  "photoUrl": "https://media.castorama.pl/media/catalog/product/cache/0/small_image/266x266/040ec09b1e35df139433887a97daa66f/O/k/Okulary_ochronne_3M_Virtua_7100140622-216647-527400.webp",
//  "description":"Okulary ochronne firmy 3M Virtua",
//  "categoryId": "category 1",
//  "keywords": "any",
//  "resources": "any"
//  };
  token: any;
  productId;
  userId;

  // tslint:disable-next-line:max-line-length
  constructor(
    private productSerevice: ProductService,
    private helpers: MyHelpers,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private rentalService: RentalService) {
   }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.token = this.helpers.getToken();
      const obj = JSON.parse(window.localStorage.token);
      console.log('ngOnInit() => window.localStorage.userId: ' + obj.userId);
      this.userId = obj.userId;
      this.productId = params.get('productId');
      this.reservationForm = this.formBuilder.group({
        userId: this.userId,
        productId: this.productId,
        start: '',
        end: ''
      });
      // this.productSerevice.getProduct(params.get('productId')).subscribe(response => {
      //   this.product = response;
      // });
      console.log('Reservation => ngOnInit productId from param = ' + params.get('productId'));
      // console.log('Reservation => ngOnInit productName from api = ' + this.productId);
    });
    setTimeout(() => {
      this.getProduct(this.productId);
      // this.getTempProduct()
    }, 200);

  }

  getProduct(id: string): void{
    this.productSerevice.getProduct(this.productId).subscribe(response => {
      this.product = response;
      console.log('Reservation => ngOnInit id from api = ' + response.id);
      console.log('Reservation => ngOnInit name from api = ' + response.name);
      console.log('Reservation => ngOnInit id from api = ' + this.product.id);
      console.log('Reservation => ngOnInit name from api = ' + this.product.name);
    });
  }

  // getTempProduct(): void {
  //   this.product = this.tempProduct;
  // }

  sendReservation(formData: any): void {
    const r = formData.value;
    // tslint:disable-next-line:max-line-length
    console.log('sendReservation() userId:' + r.userId + '  productId: ' + r.productId + '  start: ' + r.start + '  end: ' + r.end );
    // const formValues = {email: user.email, password: user.password, firstName: user.firstName, lastName: user.lastName};
    const date =  new Date(moment(r.start).utcOffset('+0000').format('YYYY-MM-DD'));
    console.log('moment: ' + date);
    // r.start = new Date(r.start).toISOString();
    // r.end = new Date(r.end).toISOString();
    r.start = new Date(moment(r.start).utcOffset('+0000', true).format('YYYY-MM-DD'));
    r.end = new Date(moment(r.end).utcOffset('+0000', true).format('YYYY-MM-DD'));
    console.log('reservation.start: ' + r.start + '  reservation.end: ' + r.end);
    const rim = new RentalInputModel(r.userId, r.productId, r.start, r.end);
    const reservationValues = {userId: r.userId, productId: r.productId, start: r.start, end: r.end};

    // this.rentalService.addRent(rim).subscribe(response => {
    //   console.log('sendReservation() => response: ' + response.code + '  ' + response.description);
    //   console.log('app-reservatioresult: ' + response);
    //   this.router.navigate(['/reservatioresult']);
    // });

    this.rentalService.addRent(rim).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('rentalService.addRent(): ' + JSON.stringify(response));
      // this.router.navigate(['/reservatioresult']);
      this.router.navigate(['reservatioresult', response]).then( (e) => {
        if (e) {
          console.log('Navigation is successful! ' + e);
        } else {
          console.log('Navigation has failed! ' + e);
        }
      });
    });
  }

}
