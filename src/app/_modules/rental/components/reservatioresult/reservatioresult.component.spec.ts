import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservatioresultComponent } from './reservatioresult.component';

describe('ReservatioresultComponent', () => {
  let component: ReservatioresultComponent;
  let fixture: ComponentFixture<ReservatioresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservatioresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservatioresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
