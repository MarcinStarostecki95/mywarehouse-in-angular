import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reservatioresult',
  templateUrl: './reservatioresult.component.html',
  styleUrls: ['./reservatioresult.component.scss']
})
export class ReservatioresultComponent implements OnInit {

  info;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const code = params.get('code');
      const message = params.get('message');
      if (code === '200'){
        console.log('code 200');
        this.info = 'RESERVATION SUCCESSFUL';
      } else {
        this.info = message;
      }
      console.log('Reservatioresult => ngOnInit = ' + code + '  ' + message);
    });
  }

}
