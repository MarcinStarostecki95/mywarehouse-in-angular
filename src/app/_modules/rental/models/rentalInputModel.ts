export class RentalInputModel {
  userId: string;
  productId: string;
  start: string;
  end: string;

  constructor(userId: string, productId: string, start: string, end: string) {
    this.userId = userId;
    this.productId = productId;
    this.start = start;
    this.end = end;
 }
}
