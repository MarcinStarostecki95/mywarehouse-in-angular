import { RentalRouting } from './rental.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductstoreservationlistComponent } from './components/productstoreservationlist/productstoreservationlist.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReservatioresultComponent } from './components/reservatioresult/reservatioresult.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSliderModule } from '@angular/material/slider';
import { MatNativeDateModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';






@NgModule({
  declarations: [ProductstoreservationlistComponent, ReservationComponent, ReservatioresultComponent],
  imports: [
    // MatToolbarModule,
    // MatButtonModule,
    // MatSidenavModule,
    // MatIconModule,
    // MatListModule,
    // MatTableModule,
    // MatPaginatorModule,
    MatFormFieldModule,
    // MatRadioModule,
    // MatSelectModule,
    // MatInputModule,
    MatDatepickerModule,
    MatSliderModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,


    CommonModule,
    RentalRouting,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [
    MatDatepickerModule
  ]
})
export class RentalModule { }
