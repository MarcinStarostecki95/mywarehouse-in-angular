import { ReservatioresultComponent } from './components/reservatioresult/reservatioresult.component';
import { ProductstoreservationlistComponent } from './components/productstoreservationlist/productstoreservationlist.component';
import { ReservationslistComponent } from './../users/components/reservationslist/reservationslist.component';
import { Routes, RouterModule } from '@angular/router';
import { ReservationComponent } from './components/reservation/reservation.component';


const RentalRoutes: Routes = [
  {
    path: 'reservatioresult',
    children: [
      { path: '', component: ReservatioresultComponent }
    ]
  },
  { path: 'reservatioresult/:response', component: ReservatioresultComponent },
  { path: 'reservation/:productId', component: ReservationComponent },
  {
    path: 'reservationlist',
    children: [
      { path: '', component: ReservationslistComponent }
    ]
  },
  {
    path: 'rental',
    children: [
      { path: '', component: ProductstoreservationlistComponent }
    ]
  }
];

export const RentalRouting = RouterModule.forChild(RentalRoutes);
