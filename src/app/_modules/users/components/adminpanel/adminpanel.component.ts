

import { Component, OnInit, Output, EventEmitter, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ReservationslistComponent } from '../reservationslist/reservationslist.component';
import { ProductslistComponent } from '../productslist/productslist.component';

@Component({
  selector: 'app-adminpanel',
  templateUrl: './adminpanel.component.html',
  styleUrls: ['./adminpanel.component.scss']
})
export class AdminpanelComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  products(): void{
    // this.router.navigate(['/adminpanel(products:aboutOne']);
    console.log('products()');
  }
  users(): void{
    // this.router.navigate(['/adminpanel(users:aboutTwo']);
    console.log('users()');
  }
  reservations(): void{
    // this.router.navigate(['/products']);
    console.log('reservations()');
    // this.reservationslistComponent.getAllReservations();
  }

}
