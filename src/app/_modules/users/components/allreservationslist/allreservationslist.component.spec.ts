import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllreservationslistComponent } from './allreservationslist.component';

describe('AllreservationslistComponent', () => {
  let component: AllreservationslistComponent;
  let fixture: ComponentFixture<AllreservationslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllreservationslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllreservationslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
