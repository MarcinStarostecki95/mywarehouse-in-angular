import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { RentalService } from 'src/app/_services/rental.service';
import { startWith, delay, first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditReservationComponent } from '../edit-reservation/edit-reservation.component';

@Component({
  selector: 'app-allreservationslist',
  templateUrl: './allreservationslist.component.html',
  styleUrls: ['./allreservationslist.component.scss']
})
export class AllreservationslistComponent implements OnInit{

  // subscription: Subscription;
  // isAdminSubscription: Subscription;
  // authentication: boolean;
  // isAdmin: boolean;
  headers = ['#', 'User', 'Photo', 'Name', 'From', 'To', 'Actions'];
  reservations: any;


  constructor(private helpers: MyHelpers, private rentalService: RentalService, private modalService: NgbModal) {

  }
  ngOnInit(): void {
    this.getAllReservations();
  }

  // ngAfterViewInit() {

  //   this.subscription = this.helpers.isAuthenticationChanged().pipe(
  //     startWith(this.helpers.isAuthenticated()),
  //     delay(0)).subscribe((value) =>
  //       this.authentication = value
  //     );

  //   this.isAdminSubscription = this.helpers.isAdminChanged().pipe(
  //       startWith(this.helpers.isAdmin()),
  //       delay(0)).subscribe((value) =>
  //       this.isAdmin = value
  //     );
  // }

  public getAllReservations(): void {
    console.log('AllreservationslistComponent => getAllReservations(): ');

    this.rentalService.getAllReservations()
      .subscribe(response => {
        this.reservations = response;
        console.log('getAllReservations(): ' + JSON.stringify(response));
      });
  }


  deleteReservation(productId: string, userId: string): void {
    console.log('deleteReservation(): ' + productId + ' ' + userId);

    this.rentalService.deleteReservation(productId, userId)
    .pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('deleteReservation() => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
        this.getAllReservations();
      }, 200);
  }

  public openModalToEdit(reservation) {
    const modalRef = this.modalService.open(EditReservationComponent);
    modalRef.componentInstance.reservation = reservation;
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    // this.subscription.unsubscribe();
    // this.isAdminSubscription.unsubscribe();
  }
}

