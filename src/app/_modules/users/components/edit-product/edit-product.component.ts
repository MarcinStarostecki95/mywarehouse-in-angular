import { MessageService } from './../../../../_services/message.service';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductService } from 'src/app/_services/product.service';
import { first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { SharedService } from 'src/app/_services/shared.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  @Input() public product;
  @Output() messageEvent = new EventEmitter<string>();
  editForm;
  message = 'message from edit-product-component';

  constructor(
    public activeModal: NgbActiveModal,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService) {
      this.editForm = this.formBuilder.group({
        id: '',
        name: '',
        photoUrl: '',
        description: ''
      });
     }

  ngOnInit(): void {

  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  update(formData: any): void {
    const product = formData.value;
    console.log(product.id + '   ' + product.name + '  ' + product.photoUrl + '  ' + product.description);
    const productValues = {productId: product.id, name: product.name, url: product.photoUrl, description: product.description};
    this.editForm.reset();
    this.productService.update(productValues).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('update => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      // this.getAll();
      this.sharedService.setEditProductEvent();
      this.closeModal();
    }, 200);
  }

}
