import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RentalService } from 'src/app/_services/rental.service';
import { FormBuilder } from '@angular/forms';
import { SharedService } from 'src/app/_services/shared.service';

@Component({
  selector: 'app-edit-reservation',
  templateUrl: './edit-reservation.component.html',
  styleUrls: ['./edit-reservation.component.scss']
})
export class EditReservationComponent implements OnInit {

  @Input() public reservation;
  editForm;

  constructor(
    public activeModal: NgbActiveModal,
    private reservationService: RentalService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService
  ) {
    this.editForm = this.formBuilder.group({
      userId: '',
      productId: '',
      start: '',
      end: ''
    });
   }

  ngOnInit(): void {
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  // "dateRange":{"start":"2020-05-26T00:00:00Z","end":"2020-05-27T00:00:00Z"}
  update(formData: any): void {
    const reservation = formData.value;
    console.log('update => response: ' + JSON.stringify(reservation));
  }

}
