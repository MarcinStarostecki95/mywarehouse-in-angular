import { FormBuilder } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/_services/user.service';
import { SharedService } from 'src/app/_services/shared.service';
import { first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  @Input() public user;
  editForm;

  constructor(
    public activeModal: NgbActiveModal,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService
  ) {
    this.editForm = this.formBuilder.group({
      id: '',
      name: '',
      lastName: '',
      email: '',
      password: ''
    });
   }

  ngOnInit(): void {
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  update(formData: any): void {
    const user = formData.value;
    console.log('updateUser => response: ' + JSON.stringify(user));
    // tslint:disable-next-line:max-line-length
    this.userService.updateUser(user.id, user.name, user.lastName, user.email, user.password).subscribe(response => {
      console.log('updateUser => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      this.sharedService.setEditUserEvent();
      this.editForm.reset();
      this.closeModal();
    }, 200);
  }

}
