import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.scss']
})
export class ModalContentComponent implements OnInit {

  @Input() public user;
  @Input() public product;

  constructor(public activeModal: NgbActiveModal  ) { }

  ngOnInit(): void {
    console.log(this.user);
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }

}
