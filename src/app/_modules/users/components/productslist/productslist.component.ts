import { EditProductComponent } from './../edit-product/edit-product.component';
import { Component, OnInit, Inject, Input, NgModule } from '@angular/core';
import { Product } from 'src/app/_modules/product/models/product';
import { FormBuilder } from '@angular/forms';
import { ProductService } from 'src/app/_services/product.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ModalContentComponent } from '../modal-content/modal-content.component';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/_services/shared.service';



@Component({
  selector: 'app-productslist',
  templateUrl: './productslist.component.html',
  styleUrls: ['./productslist.component.scss']
})
export class ProductslistComponent implements OnInit {

  headers = ['#', 'Photo', 'Name', 'Description', 'Actions'];
  products: any;
  selectedProduct: Product;
  productToEdit: Product;
  editForm;
  deleteForm;
  closeResult: string;
  mySubscription: any;

  dp;
  content;
  public user = {name: ' Izzat Nadiri ', age: 26};
  public product =  {id: '123431234 ', name: 'product test', description: 'opis'};

  editProductSubscription: Subscription;

  constructor(
    private sharedService: SharedService,
    private productService: ProductService,
    private router: Router,
    private formBuilder: FormBuilder,
    @Inject(DOCUMENT) private document: Document,
    private modalService: NgbModal) {

      this.editForm = this.formBuilder.group({
        id: '',
        name: '',
        photoUrl: '',
        description: ''
      });

      this.deleteForm = this.formBuilder.group({
        id: '',
        name: '',
        photoUrl: '',
        description: ''
      });

      this.editProductSubscription = this.sharedService.getEditProductEvent().subscribe(() => {
        this.getAll();
        });
     }

  ngOnInit(): void {
    this.getAll();
  }

  public getAll(): void {
    this.productService.getAll()
      .subscribe(response => {
        this.products = response.products;
      });
  }

  public onSelect(item: any): void {
    this.selectedProduct = item;
    console.log('onSelect()' + this.selectedProduct.name);
  }

  public update(formData: any): void {
    const product = formData.value;
    console.log(product.id + '   ' + product.name + '  ' + product.photoUrl + '  ' + product.description);
    const productValues = {productId: product.id, name: product.name, url: product.photoUrl, description: product.description};
    this.editForm.reset();
    this.productService.update(productValues).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('update => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      this.getAll();
    }, 500);
  }

  public delete(product: any): void {
    // const product = formData.value;
    console.log(product.id + '   ' + product.name + '  ' + product.photoUrl + '  ' + product.description);
    this.editForm.reset();
    this.productService.delete(product.id).pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('delete => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
      // this.products = this.products.filter(p => p.id !== product.id);
      this.getAll();
    }, 300);
  }

  public editProduct(p: any): void {
    console.log('editProdict() ' + JSON.stringify(p));
  }

  public openModalToEdit(product) {
    const modalRef = this.modalService.open(EditProductComponent);
    modalRef.componentInstance.product = product;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.editProductSubscription.unsubscribe();
  }
}
