import { RentalService } from 'src/app/_services/rental.service';
import { ProductService } from 'src/app/_services/product.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { startWith, delay, first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements AfterViewInit {

  subscription: Subscription;
  isAdminSubscription: Subscription;
  authentication: boolean;
  isInRoleAdmin: boolean;
  headers = ['#', 'Photo', 'Name', 'From', 'To', 'Actions'];
  reservations: any;


  constructor(private helpers: MyHelpers, private rentalService: RentalService) {

  }

  ngAfterViewInit() {

    this.subscription = this.helpers.isAuthenticationChanged().pipe(
      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );

    this.getOwnReservations();
  }


  getOwnReservations(): void {
    // console.log('getOwnReservations()');
    this.rentalService.getOwnReservations()
      .subscribe(response => {
        this.reservations = response;
        console.log('getOwnReservations(): ' + JSON.stringify(response));
      });

  }

  deleteReservation(productId: string, userId: string): void {
    console.log('deleteReservation(): ' + productId + ' ' + userId);

    this.rentalService.deleteReservation(productId, userId)
    .pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('deleteReservation() => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
        this.getOwnReservations();
      }, 200);
  }

  deleteProduct(productId: string, userId: string): void {
    console.log('deleteReservation(): ' + productId + ' ' + userId);

    this.rentalService.deleteReservation(productId, userId)
    .pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('deleteReservation() => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
        this.getOwnReservations();
      }, 200);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscription.unsubscribe();
    // this.isAdminSubscription.unsubscribe();
  }
}
