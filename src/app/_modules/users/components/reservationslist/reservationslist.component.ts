import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { RentalService } from 'src/app/_services/rental.service';
import { startWith, delay, first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-reservationslist',
  templateUrl: './reservationslist.component.html',
  styleUrls: ['./reservationslist.component.scss']
})
export class ReservationslistComponent implements AfterViewInit{

  subscription: Subscription;
  isAdminSubscription: Subscription;
  authentication: boolean;
  isAdmin: boolean;
  headers = ['#', 'Photo', 'Name', 'From', 'To', 'Actions'];
  reservations: any;


  constructor(private helpers: MyHelpers, private rentalService: RentalService) {

  }

  ngAfterViewInit() {

    this.subscription = this.helpers.isAuthenticationChanged().pipe(
      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );

    this.isAdminSubscription = this.helpers.isAdminChanged().pipe(
        startWith(this.helpers.isAdmin()),
        delay(0)).subscribe((value) =>
        this.isAdmin = value
      );

    this.getOwnReservations();
  }

  public getAllReservations(): void {
    this.rentalService.getAllReservations()
      .subscribe(response => {
        this.reservations = response;
        console.log('getAllReservations(): ' + JSON.stringify(response));
      });
  }

  getOwnReservations(): void {
    this.rentalService.getOwnReservations()
      .subscribe(response => {
        this.reservations = response;
        console.log('getOwnReservations(): ' + JSON.stringify(response));
      });
  }

  deleteReservation(productId: string, userId: string): void {
    console.log('deleteReservation(): ' + productId + ' ' + userId);

    this.rentalService.deleteReservation(productId, userId)
    .pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('deleteReservation() => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
        this.getOwnReservations();
      }, 200);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.isAdminSubscription.unsubscribe();
  }
}
