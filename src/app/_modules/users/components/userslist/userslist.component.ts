import { UserService } from './../../../../_services/user.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyHelpers } from 'src/app/_helpers/MyHelpers';
import { RentalService } from 'src/app/_services/rental.service';
import { startWith, delay, first } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedService } from 'src/app/_services/shared.service';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent implements AfterViewInit {

  isAuthenticatedSubscription: Subscription;
  isAdminSubscription: Subscription;
  editUserSubscription: Subscription;
  authentication: boolean;
  isInRoleAdmin: boolean;
  headers = ['#', 'First Name', 'Last Name', 'Email', 'Is Active', 'Actions'];
  users: any;



  constructor(
    private sharedService: SharedService,
    private helpers: MyHelpers,
    private rentalService: RentalService,
    private userService: UserService,
    private modalService: NgbModal) {

  }

  ngAfterViewInit() {

    this.isAuthenticatedSubscription = this.helpers.isAuthenticationChanged().pipe(
      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );

    this.editUserSubscription = this.sharedService.getEditUserEvent().subscribe(() => {
      this.getAllUsers();
      });

    this.getAllUsers();
  }


  getAllUsers(): void {
    this.userService.getAllUsers()
      .subscribe(response => {
        this.users = response.users;
        console.log('getAllUsers(): ' + JSON.stringify(response));
      });

  }

  deleteUser(userId: string): void {
    console.log('deleteUser: ' + userId);
    this.userService.deleteUser(userId)
    .pipe(first()).subscribe((response: HttpResponse<any>) => {
      console.log('deleteUser() => response: ' + JSON.stringify(response));
    });
    setTimeout(() => {
        this.getAllUsers();
      }, 200);
  }

  public openModalToEdit(user) {
    const modalRef = this.modalService.open(EditUserComponent);
    modalRef.componentInstance.user = user;
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.isAuthenticatedSubscription.unsubscribe();
    // this.isAdminSubscription.unsubscribe();
  }
}
