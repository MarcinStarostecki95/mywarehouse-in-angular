import { UsersRouting } from './users.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminpanelComponent } from './components/adminpanel/adminpanel.component';
import { UserslistComponent } from './components/userslist/userslist.component';
import { ProductslistComponent } from './components/productslist/productslist.component';
import { ReservationslistComponent } from './components/reservationslist/reservationslist.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllreservationslistComponent } from './components/allreservationslist/allreservationslist.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { ModalContentComponent } from './components/modal-content/modal-content.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditReservationComponent } from './components/edit-reservation/edit-reservation.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [
    ProfileComponent,
    AdminpanelComponent,
    UserslistComponent,
    ProductslistComponent,
    ReservationslistComponent,
    AllreservationslistComponent,
    ModalContainerComponent,
    ModalContentComponent,
    EditProductComponent,
    EditUserComponent,
    EditReservationComponent
  ],
  imports: [
    CommonModule,
    UsersRouting,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbModule
  ],
  entryComponents: [ ModalContentComponent ]
})
export class UsersModule { }
