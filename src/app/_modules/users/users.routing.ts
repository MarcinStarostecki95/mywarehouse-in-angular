import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { UserslistComponent } from './components/userslist/userslist.component';
import { AdminpanelComponent } from './components/adminpanel/adminpanel.component';
import { ProfileComponent } from './components/profile/profile.component';
import { Routes, RouterModule } from '@angular/router';
import { ProductslistComponent } from './components/productslist/productslist.component';
import { ReservationslistComponent } from './components/reservationslist/reservationslist.component';
import { AllreservationslistComponent } from './components/allreservationslist/allreservationslist.component';


const usersRoutes: Routes = [
  {
    path: 'profile',
    children: [
      { path: '', component: ProfileComponent }
    ]
  },
  {
    path: 'adminpanel',
    component: AdminpanelComponent,
    children: [
        {
            path: '',
            outlet: 'productsOutlet',
            component: ProductslistComponent,
            pathMatch: 'full'
        },
        {
            path: '',
            outlet: 'usersOutlet',
            component: UserslistComponent,
            pathMatch: 'full'
        },
        {
          path: '',
          outlet: 'reservationsOutlet',
          component: AllreservationslistComponent,
          pathMatch: 'full'
        }
    ]
},
  {
    path: 'userslist',
    children: [
      { path: '', component: UserslistComponent }
    ]
  },
  {
    path: 'modal-container',
    children: [
      { path: '', component: ModalContainerComponent }
    ]
  },
  {
    path: 'allreservations',
    children: [
      { path: '', component: AllreservationslistComponent }
    ]
  }

];

export const UsersRouting = RouterModule.forChild(usersRoutes);
