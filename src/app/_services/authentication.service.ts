import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { MessageService } from './message.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private messageService: MessageService) { }

  login(data: any): any {
    const body = JSON.stringify(data);
    return this.loginPost(body);
  }

  register(data: any): any {
    const body = JSON.stringify(data);
    return this.registerPost(body);
  }


  private loginPost(body: any): Observable<any> {
    return this.http.post<any>('http://localhost:56119/api/Authentication/Login', body, httpOptions).pipe(
        catchError(this.handleError)
      );
  }

  private registerPost(body: any): Observable<any> {
    return this.http.post<any>('http://localhost:56119/api/Authentication/register', body, httpOptions).pipe(
        catchError(this.handleError)
      );
  }


private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

 /** Log a Service message with the MessageService */
 private log(message: string) {
  this.messageService.add('HeroService: ' + message);
}
}


