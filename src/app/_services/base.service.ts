import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { MyHelpers } from '../_helpers/MyHelpers';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(private helper: MyHelpers, private messageService: MessageService) { }

  public header() {
    let header = new HttpHeaders({ 'Content-Type': 'application/json' });
    if (this.helper.isAuthenticated()) {
      const token = this.helper.getToken();
      console.log('BaseService => token.token ' + token);
      header = header.append('Authorization', 'Bearer ' + token);
    }
    return { headers: header };
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

   /** Log a Service message with the MessageService */
   public log(message: string) {
    this.messageService.add('MessageService: ' + message);
  }
}
