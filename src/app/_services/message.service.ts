import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: string[] = [];
  private listners = new Subject<any>();

  constructor() { }

  public add(message: string){
    this.messages.push(message);
  }

  public clear(){
    this.messages = [];
  }

  listen(): Observable<any> {
    return this.listners.asObservable();
 }

 filter(filterBy: string) {
    this.listners.next(filterBy);
 }
}
