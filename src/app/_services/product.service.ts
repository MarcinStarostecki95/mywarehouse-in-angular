import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { MessageService } from './message.service';
import { MyHelpers } from '../_helpers/MyHelpers';
import { BaseService } from './base.service';
import { Product } from '../_modules/product/models/product';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService {

  constructor(private http: HttpClient, messageService: MessageService, helper: MyHelpers) { super(helper, messageService); }

    getAll(): Observable<any> {
      return this.http.get<any>('http://localhost:56119/api/Product/GetAllProduct', httpOptions)
        .pipe(
          catchError(super.handleError)
        );
    }

    GetRandomProduct(count: number): Observable<any> {
      return this.http.get<any>(`http://localhost:56119/api/Product/GetRandomProduct?count=${count}`, httpOptions)
        .pipe(
          catchError(super.handleError)
        );
    }

    getProduct(id: string): Observable<Product> {
      const url = `http://localhost:56119/api/Product/GetProduct?productId=${id}`;
      return this.http.get<Product>(url, httpOptions)
        .pipe(
          tap(_ => this.log(`fetched product id=${id}`)),
          catchError(super.handleError<Product>(`getProduct id=${id}`))
        );
    }

    searchProduct(query: string, categoryId: string): Observable<any> {
      return this.http.get(`http://localhost:56119/api/Product/SearchProduct?query=${query}&categoryId=${categoryId}`, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    getProductById(id: string): Observable<Product> {
      const rootUrl = 'http://';
      const url = `${rootUrl}/${id}`;
      return this.http.get<Product>(url).pipe(
        tap(_ => this.log(`fetched product id=${id}`)),
        catchError(super.handleError<Product>(`getProductById id=${id}`))
      );
    }

    // insert
    add(data: any): Observable<any> {
      return this.http.post('http://localhost:56119/api/Product/AddProduct', JSON.stringify(data), httpOptions)
        .pipe(
          catchError(super.handleError)
        );
    }

    // update
    update(data: any): Observable<any> {
      const url = `http://localhost:56119/api/Product/UpdateProduct?productId=${data.productId}&name=${data.name}&url=${data.url}&description=${data.description}`;
      return this.http.put(url, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    // delete
    delete(id: string): Observable<any> {
      const u = `http://localhost:56119/api/Product/DeleteProduct?productId=${id}`;
      const newurl = `${u}?id=${id}`;
      const url = `${'http://localhost:56119/api/Product/DeleteProduct'}/${id}`; // DELETE api/contact/42
      return this.http.delete(u, httpOptions)
        .pipe(
          catchError(super.handleError)
        );
    }

    // private handleError<T>(operation = 'operation', result?: T) {
    //   return (error: any): Observable<T> => {

    //     // TODO: send the error to remote logging infrastructure
    //     console.error(error); // log to console instead

    //     // TODO: better job of transforming error for user consumption
    //     this.log(`${operation} failed: ${error.message}`);

    //     // Let the app keep running by returning an empty result.
    //     return of(result as T);
    //   };
    // }

    //  /** Log a Service message with the MessageService */
    //  private log(message: string) {
    //   super.messageService.add('HeroService: ' + message);
    // }
}
