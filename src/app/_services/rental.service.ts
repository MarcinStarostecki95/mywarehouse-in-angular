import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { MessageService } from './message.service';
import { MyHelpers } from '../_helpers/MyHelpers';
import { BaseService } from './base.service';
import { Product } from '../_modules/product/models/product';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RentalService extends BaseService {

  constructor(private http: HttpClient, messageService: MessageService, helper: MyHelpers) { super(helper, messageService); }

    getAllReservations(): Observable<any> {
      return this.http.get<any>('http://localhost:56119/api/Rental/GetAllReservations', super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    // "userId": "5ebe6f9ffefd7d3f749199aa",
    // "productId": "5eac0ff098aada40e4411770",
    // "productName": "Obcinak do rur Top Tools",
    // "dateRange": {
    //   "start": "2020-05-22T00:00:00Z",
    //   "end": "2020-05-30T00:00:00Z"
    // },
    // "imageUrl": "https://media.castinak_do_rur_Top_Tools_3_-42_mm-216043-505504.webp",
    // "price": 0,
    // "userName": "jan nowak"
    getOwnReservations(): Observable<any> {
      return this.http.get<any>('http://localhost:56119/api/Rental/GetOwnReservations', super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    addRent(data: any): Observable<any> {
      return this.http.post('http://localhost:56119/api/Rental/AddRent', JSON.stringify(data), super.header())
        .pipe(
          catchError(super.handleError)
        );
    }


    deleteReservation(productId: string, userId: string): Observable<any> {
      return this.http.delete(`http://localhost:56119/api/Rental/DeleteReservation?productId=${productId}&userId=${userId}`, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }
}
