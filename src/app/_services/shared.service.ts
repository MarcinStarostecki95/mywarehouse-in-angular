import { Injectable } from '@angular/core';;
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private editProductSubject = new Subject<any>();
  private editUserSubject = new Subject<any>();

  constructor() { }

  setEditProductEvent() {
    this.editProductSubject.next();
  }

  getEditProductEvent(): Observable<any> {
    return this.editProductSubject.asObservable();
  }

  setEditUserEvent() {
    this.editUserSubject.next();
  }

  getEditUserEvent(): Observable<any> {
    return this.editUserSubject.asObservable();
  }
}
