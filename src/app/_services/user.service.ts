import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { MessageService } from './message.service';
import { MyHelpers } from '../_helpers/MyHelpers';
import { BaseService } from './base.service';
import { Product } from '../_modules/product/models/product';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  constructor(private http: HttpClient, messageService: MessageService, helper: MyHelpers) { super(helper, messageService); }

    getAllUsers(): Observable<any> {
      return this.http.get<any>('http://localhost:56119/api/Users/GetAllUsers', super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    deleteUser(userId: string): Observable<any> {
      return this.http.delete(`http://localhost:56119/api/Users/DeleteUser?userId=${userId}`, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    // {
    //   "userId": "string",
    //   "firstName": "string",
    //   "lastName": "string",
    //   "avatarUrl": "string",
    //   "email": "string"
    // }
    updateUserProfile(data: any): Observable<any> {
      const body = JSON.stringify(data);
      return this.http.put('http://localhost:56119/api/Users/UpdateUserProfile', body, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }

    updateUser(userId: string, firstName: string, lastName: string, email: string, password: string): Observable<any> {
      const url = `http://localhost:56119/api/Users/UpdateUser?userId=${userId}&firstName=${firstName}&lastName=${lastName}&email=${email}&password=${password}`;
      return this.http.put(url, super.header())
        .pipe(
          catchError(super.handleError)
        );
    }
}
