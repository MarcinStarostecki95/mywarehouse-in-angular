import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ProductListComponent } from './_modules/product/components/product-list/product-list.component';


const routes: Routes = [
  { path: '', redirectTo: '/rental', pathMatch: 'full' },
  { path: 'productlist', component: ProductListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
