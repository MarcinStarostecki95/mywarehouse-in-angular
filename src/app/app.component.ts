import { Component, AfterViewInit } from '@angular/core';
import { startWith, delay } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MyHelpers } from './_helpers/MyHelpers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  subscription: Subscription;
  isAdminSubscription: Subscription;
  authentication: boolean;
  isInRoleAdmin: boolean;
  title = 'Angular 5 Seed';

  constructor(private helpers: MyHelpers) {

  }

  ngAfterViewInit() {

    this.subscription = this.helpers.isAuthenticationChanged().pipe(

      startWith(this.helpers.isAuthenticated()),
      delay(0)).subscribe((value) =>
        this.authentication = value
      );

    // this.isAdminSubscription = this.helpers.isRoleChanged().pipe(
    //     startWith(this.helpers.isInRole('Admin')),
    //     delay(0)).subscribe((value) =>
    //     this.isInRoleAdmin = value
    //   );
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscription.unsubscribe();
    // this.isAdminSubscription.unsubscribe();
  }
}
