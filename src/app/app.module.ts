import { MatDatepickerModule } from '@angular/material/datepicker';
import { RentalModule } from './_modules/rental/rental.module';
import { UsersModule } from './_modules/users/users.module';
import { ProductModule } from './_modules/product/product.module';
import { ProductRouting } from './_modules/product/product.routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './_modules/authentication/authentication.module';
import { NavComponent } from './_components/nav/nav.component';
import { ContentComponent } from './_components/content/content.component';
import { FooterComponent } from './_components/footer/footer.component';
import { AuthGuard } from './_helpers/AuthGuard';
import { MyHelpers } from './_helpers/MyHelpers';
import { AuthenticationService } from './_services/authentication.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatNativeDateModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ContentComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AuthenticationModule,
    ProductModule,
    UsersModule,
    RentalModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule
  ],
  exports: [
    MatDatepickerModule
  ],
  providers: [
    MyHelpers, AuthGuard, AuthenticationService, MatDatepickerModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
